data BinaryTree a = EmptyNode | Node (BinaryTree a) (BinaryTree a) a

printTree :: Show a => BinaryTree a -> String
printTree EmptyNode = "*"
printTree x = printTreeH 5 x

printTreeH :: (Show a) => Int -> BinaryTree a -> String
printTreeH n EmptyNode = (replicate n ' ') ++ "*"
printTreeH n x @ (Node left right a) = (replicate n ' ') ++
  (show a) ++ "\n" ++ (printTreeH (n+1) left) ++ "\n" ++ (printTreeH (n+1) right)


tree = Node (Node EmptyNode EmptyNode 2) EmptyNode 1

main :: IO()
main = 
  putStrLn (printTree tree)
