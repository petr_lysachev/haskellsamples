
type SName = String
type SSurname = String
type Course = Int
type Group = Int
type Marks = Float

type Student = (String, String, Int, Int, Float)
type StudentList = [Student]

listStudents :: StudentList
listStudents = [("Ivan", "Sidorov", 3, 2, 4.5), 
                ("Svetlana", "Petrova", 2, 5, 4.8), 
                ("Igor", "Smirnov", 4, 2, 4.5)]

avgMarks :: StudentList -> Float
avgMarks l = (foldr (\st acc -> getMarks st + acc) 0 l) / fromIntegral(lengthOfList'(l))

lengthOfList' :: StudentList -> Int
lengthOfList' xs = foldr (\l acc -> acc + 1) 0 xs

getMarks :: Student -> Float
getMarks (_, _, _, _, m) = m


main :: IO()
main = print(avgMarks(listStudents))