type Student = (String, String, Int, Int, Float)
type StudentList = [Student]

aStudents :: StudentList
aStudents = [ ("Ivan", "Ivanov", 3, 1, 3.4)
             ,("Svetlana", "Petrova", 3, 1, 4.1)]

sumMarks :: StudentList -> Float
sumMarks [] = 0.0
sumMarks (s:xs) = getMarks(s) + sumMarks(xs)

sumMarks' :: StudentList -> Float
sumMarks' xs = foldr getMarksAndAdd 0.0 xs

getMarksAndAdd :: Student -> Float -> Float
getMarksAndAdd s total = getMarks(s) + total

getMarks :: Student -> Float
getMarks (_, _, _, _, m) = m

main::IO()
main = putStrLn("Sum of marks = " ++ show(sumMarks' aStudents) )