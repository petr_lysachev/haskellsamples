import Data.List
import Data.Maybe
import Data.List.Split
import Control.Monad
import Text.Read

-- X 0 .

-- ...
-- ...
-- ...

-- input coordinates

type GameState = [[Char]]

initialState :: GameState
initialState = ["...","...","..."]

test1State :: GameState
test1State = ["X..","XXX","..."]

test2State :: GameState
test2State = ["X..","XXX","OOO"]

test3State :: GameState
test3State = ["X..","XXX","XOO"]

test4State :: GameState
test4State = ["XO.","XOX","OOO"]

test5State :: GameState
test5State = ["X..",".X.","OOX"]

test6State :: GameState
test6State = ["X.O","XOX","OOO"]


joinNewLine :: String -> String -> String           
joinNewLine a b =
  a ++ "\n" ++ b
    
showGameState :: GameState -> IO()
showGameState state = do
  putStrLn (foldl joinNewLine "" state)
  
printIntro :: IO()
printIntro = do
  putStrLn ("Mega game X-0. Your move is first.")

printMoveInstr :: IO()
printMoveInstr = do
  putStrLn ("Input coordinates of next X: a b where a is a line, b is a column. a, b = [1..3]")

readMoveAndApply :: GameState -> IO()
readMoveAndApply state = do
  showGameState state
  printMoveInstr

  coords <- getLine
  let coordsList = splitOn " " coords
  let mb_a = readMaybe (coordsList !! 0) :: Maybe Int
  let a = case mb_a of
               Nothing -> 0
               Just n -> n - 1
  
  if (a < 0 || a > 2) then do
    putStrLn("First string component should be 1..3")
    readMoveAndApply state
  else 
    putStrLn("OK")

  let mb_b = readMaybe (coordsList !! 1) :: Maybe Int
  let b = case mb_b of
               Nothing -> 0
               Just n -> n - 1

  if (b < 0 || b > 2) then do
    putStrLn("Second string component should be 1..3")
    readMoveAndApply state
  else
    if (state !! a) !! b == '.' then do
      mainCycle (replaceAt state a b 'X') 'O'
    else do     
      putStrLn("There is " ++ [((state !! a) !! b)] ++ " on these coordinates")
      readMoveAndApply state

replaceAt :: GameState -> Int -> Int -> Char -> GameState
replaceAt state a b c =
  take a state ++ [(replaceAtList (state!!a) b c)] ++ drop (a + 1) state
  where 
    replaceAtList lst b c = take b lst ++ [c] ++ drop (b + 1) lst

hor :: GameState -> Int -> Char -> Char
hor state i c = 
  if all (== c) (state !! i) then c
    else '.'

vert :: GameState -> Int -> Char -> Char
vert state i c = 
  if all (\h -> ((state !! h) !! i) == c) [0..2] then c
    else '.'

diag :: GameState -> Char -> Char
diag state c = 
  if all (\h -> ((state !! h) !! h) == c) [0..2] 
    || all (\h -> ((state !! h) !! (2 - h)) == c) [0..2]
      then c
      else '.'

gameOver :: GameState -> Char -> Bool
gameOver state c = 
  any (\i -> hor state i c == c) [0..2] 
  || any (\i -> vert state i c == c) [0..2]
  || (diag state c == c)
  
draw :: GameState -> Bool
draw state =
  notElem '.' (concat state)

wantToPlayAgain :: IO Bool
wantToPlayAgain = do
  putStrLn "Wanna play again? y/n"
  cmd <- getLine
  case cmd of
    "y" -> return True
    "n" -> return False
    _ -> wantToPlayAgain 

showComputerVictory :: IO()
showComputerVictory = putStrLn "Computer won"

showPlayerVictory :: IO()
showPlayerVictory = putStrLn "You won"

checkIfWinMove :: GameState -> Char -> Int -> Bool
checkIfWinMove state c index =
  ((state !! (quot index 3)) !! (mod index 3) == '.') &&
  gameOver (replaceAt state (quot index 3) (mod index 3) c) c
  
computerMove :: GameState -> IO()
computerMove state = do
  let mayBeIndex = find (checkIfWinMove state 'O') [0..8]        
  let defaultIndex = fromMaybe (fromJust ( elemIndex '.' (concat state))) (find (checkIfWinMove state 'X') [0..8])
  let index = fromMaybe (defaultIndex) mayBeIndex 
  mainCycle (replaceAt state (quot index 3) (mod index 3) 'O') 'X'


mainCycle :: GameState -> Char -> IO()
mainCycle gameState c = do          
  if (gameOver gameState 'X') 
    then do
      showPlayerVictory
      playAgain <- wantToPlayAgain
      if playAgain then
        mainCycle initialState 'X'
      else
        putStrLn "Thank you. Goodbye!"
    else
      if (gameOver gameState 'O') 
        then do
          showComputerVictory
          playAgain <- wantToPlayAgain
          if playAgain then
            mainCycle initialState 'X'
          else
            putStrLn "Thank you. Goodbye!"
        else
          if draw gameState then do
            putStrLn "Draw!"
            playAgain <- wantToPlayAgain
            if playAgain then
              mainCycle initialState 'X'
            else
              putStrLn "Thank you. Goodbye!"
          else
            if c == 'X' then
              readMoveAndApply gameState
            else
              computerMove gameState

main :: IO()

main = do
  printIntro
  mainCycle initialState 'X'
  