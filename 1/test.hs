main = do
  let n = 123
  print n
  putStrLn "1"

  let p = (\x -> x * x) 25
  print p

  let sqr = \x -> x * x
  let p1 = sqr 2
  print p1

