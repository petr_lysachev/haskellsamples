import Data.Function (fix)

func :: Integer -> Integer
func = \x -> 
  if x <= 2 then x
  else x + func(x - 1)

main = do
  let m = func 4
  print(m)
  
  let m1 = fix(\ff -> (\x -> if x <= 2 then x else x + ff(x-1))) 4
  print(m1)