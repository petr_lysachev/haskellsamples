p :: Integer
p = 25

q = (\x -> x * x) p

sqr :: Integer -> Integer
sqr = \x -> x * x

q2 = (\x y -> x * x + y * y) 3 2

norma :: (Integer, Integer) -> Integer
norma = \(x, y) -> x * x + y * y

normaC = curry norma

main = do
  --print(q)
  --print(q2)

  print(sqr p)
  print( norma (3,2) )
  print( normaC 3.2 2 )
