(^&^) :: (Num a) => a -> a -> a
(^&^) a1 a2 = a1 * a1 + a2 * a2
infixl 7 ^&^

ff a1 a2 = a1 * a1 + a2 * a2


main :: IO()
main = print (4 ^&^ 3)
--main = print( 5 `ff` 6)