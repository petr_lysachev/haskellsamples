import qualified Data.List as L
import Data.Maybe

type SName = String
type SSurname = String
type CourseNum = Int
type GroupNum = Int
type Marks = Float

data StudentType = Bachelor | Magister deriving Show               --sum  = ����������
data Student = Student SName SSurname Marks    --product = ����������
data Group = Group GroupNum [Student]
data Course = Course CourseNum [Group]
data FacultyDivision = FacultyDivision StudentType [Course]
type Faculty = [FacultyDivision]

faculty :: Faculty 
faculty = [
            FacultyDivision Bachelor [
                                       Course 1 [
                                                   Group 1 [
                                                      Student "Ivan" "Petrov" 3.4,
                                                      Student "Elena" "Ivanova" 4.2
                                                   ],
                                                   Group 2 [
                                                      Student "Sergey" "Petrov" 3.6,
                                                      Student "Mikhail" "Ivanov" 4.5
                                                   ],
                                                   Group 4 [
                                                      Student "Ivan" "Sidorov" 4.4,
                                                      Student "Elena" "Belaya" 3.2
                                                   ]
                                                ], 
                                       Course 2 [], 
                                       Course 3 [], 
                                       Course 4 []
                                     ],
            FacultyDivision Magister [
                                       Course 1 [
                                                   Group 1 [
                                                      Student "Ivan" "Petrov" 3.4,
                                                      Student "Dasha" "Ivanova" 5.0
                                                   ],
                                                   Group 2 [
                                                      Student "Sergey" "Petrov" 4.1,
                                                      Student "Mikhail" "Ivanov" 4.5
                                                   ],
                                                   Group 4 [
                                                      Student "Ivan" "Sidorov" 3.4,
                                                      Student "Elena" "Belaya" 4.2
                                                   ]
                                                ], 
                                       Course 2 []
                                     ]
          ]


testGroup = Group 1 [
                    Student "Ivan" "Petrov" 3.4,
                    Student "Elena" "Ivanova" 4.2
                  ]

getMarks :: Student -> Float
getMarks (Student _ _ m) = m

findBestFrom :: [Student] -> Maybe Student
findBestFrom [] = Nothing
findBestFrom s = Just (head (sortStudents s))
  where
    sortStudents s = L.sortBy compare2Students s
    compare2Students s1 s2 = compare (getMarks(s2)) (getMarks(s1))

findBestFromMaybe :: [Maybe Student] -> Maybe Student
findBestFromMaybe s = findBestFrom(map fromJust (filter isJust s)) 

findBestFromGroup :: Group -> Maybe Student
findBestFromGroup (Group _ students) = findBestFrom students
     
findBestFromCourse :: Course -> Maybe Student
findBestFromCourse (Course _ groups) = findBestFromMaybe (map findBestFromGroup groups)

findBestFromDiv :: FacultyDivision -> Maybe Student
findBestFromDiv (FacultyDivision _ courses) = findBestFromMaybe (map findBestFromCourse courses)

findBestFromFaculty :: Faculty -> Maybe Student
findBestFromFaculty f = findBestFromMaybe (map findBestFromDiv f)


main :: IO()
main = do
  let s = fromJust (findBestFromFaculty faculty)
  print ((\(Student n s m) -> n ++ " " ++ s ++ " " ++ show(m)) s)
  