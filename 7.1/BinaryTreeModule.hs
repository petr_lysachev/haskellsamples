module BinaryTreeModule where

data BinaryTree a = EmptyNode | Node a (BinaryTree a) (BinaryTree a) deriving Show

printTree :: (Show a) => BinaryTree a -> Int -> String
printTree x indent = printTreeIndent 0 x indent

printTreeIndent :: (Show a) => Int -> BinaryTree a -> Int -> String
printTreeIndent n EmptyNode indent = (replicate (n*indent) ' ') ++ "*"
printTreeIndent n (Node a left right) indent = (replicate (n*indent) ' ') ++ show(a) 
  ++ "\n" 
  ++ (printTreeIndent (n + 1) left indent) ++ "\n" 
  ++ (printTreeIndent (n + 1) right indent)  

