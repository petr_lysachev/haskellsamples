import BinaryTreeModule

type Name = String
type Hours = Int
data Task = Task Name Hours 

instance Show Task where
  show (Task name 0) = name
  show (Task name hours) = name ++ " (" ++ show(hours) ++ " h)"

tree :: BinaryTree Task
tree = Node (Task "Project Bicycles" 0) 
            (Node (Task "Initiation" 0) 
                  (Node (Task "Team gathering" 8) EmptyNode EmptyNode
                  )
                  (Node (Task "Environment configuration" 24) EmptyNode EmptyNode 
                  )
             )
            (Node (Task "Development" 0) 
                  (Node (Task "UI" 80) EmptyNode EmptyNode
                  )
                  (Node (Task "Backend" 323) EmptyNode EmptyNode 
                  )
             )

projectHours :: BinaryTree Task -> Hours
projectHours EmptyNode = 0
projectHours (Node t@(Task n h) left right) = h + projectHours left + projectHours right 

projectHours' :: BinaryTree Task -> String
projectHours' x = show(projectHours(x))

printTreeX :: (Show a) => (BinaryTree a -> String) -> BinaryTree a -> Int -> String
printTreeX func x indent = printTreeIndentX func 0 x indent

printTreeIndentX :: (Show a) => (BinaryTree a -> String) -> Int -> BinaryTree a -> Int -> String
printTreeIndentX f n EmptyNode indent = (replicate (n*indent) ' ') ++ "*"
printTreeIndentX f n tree@(Node a left right) indent = (replicate (n*indent) ' ') ++ show(a) 
  ++ " " ++ f(tree)
  ++ "\n" 
  ++ (printTreeIndentX f (n + 1) left indent) ++ "\n" 
  ++ (printTreeIndentX f (n + 1) right indent)  



main :: IO()
main = do
  putStrLn(printTreeX projectHours' tree 4)
  putStrLn("Total hours = " ++ show(projectHours tree))

