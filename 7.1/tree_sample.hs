import qualified BinaryTreeModule

tree :: BinaryTreeModule.BinaryTree Int
tree = BinaryTreeModule.Node 123 (BinaryTreeModule.Node 22 BinaryTreeModule.EmptyNode BinaryTreeModule.EmptyNode) 
                (BinaryTreeModule.Node 55 (BinaryTreeModule.Node 33 BinaryTreeModule.EmptyNode BinaryTreeModule.EmptyNode) BinaryTreeModule.EmptyNode)

type Name = String
type Hours = Int
data Task = Task Name Hours

main :: IO()

main =
  putStrLn(BinaryTreeModule.printTree tree 4)

